import torch
from pytorch_pretrained_bert import BertTokenizer, BertModel, BertForMaskedLM

# OPTIONAL: if you want to have more information on what's happening, activate the logger as follows
import logging
logging.basicConfig(level=logging.INFO)

# Load pre-trained model tokenizer (vocabulary)
tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')

# Load pre-trained model (weights)
model = BertForMaskedLM.from_pretrained('bert-base-uncased')
model.eval()


with open('sentences.txt') as sentences:
    # vocab = [word for word in line.strip('\n').split() for line in sentences]
    count = 0
    i = 0
    for line in sentences:        
        # if i > 10: break
        # Tokenized input
        # text = "[CLS] Who was Jim Henson ? [SEP] Jim Henson was a puppeteer [SEP]"
        # text = "[CLS] candy is to sweets is like sofa is to chair ."
        text ="[CLS] "+line.strip('\n')+" [SEP]"
        tokenized_text = tokenizer.tokenize(text)

        # Mask a token that we will try to predict back with `BertForMaskedLM`
        masked_index = -3
        gold = tokenized_text[masked_index]
        tokenized_text[masked_index] = '[MASK]'
        # print(tokenized_text)
        # assert tokenized_text == ['[CLS]', 'who', 'was', 'jim', 'henson', '?', '[SEP]', 'jim', '[MASK]', 'was', 'a', 'puppet', '##eer', '[SEP]']

        # Convert token to vocabulary indices
        indexed_tokens = tokenizer.convert_tokens_to_ids(tokenized_text)
        # Define sentence A and B indices associated to 1st and 2nd sentences (see paper)
        segments_ids = [0]*len(tokenized_text)

        # Convert inputs to PyTorch tensors
        tokens_tensor = torch.tensor([indexed_tokens])
        segments_tensors = torch.tensor([segments_ids])

        # Predict all tokens
        with torch.no_grad():
            predictions = model(tokens_tensor, segments_tensors)

        # confirm we were able to predict 'henson'
        predicted_index = torch.argmax(predictions[0, masked_index]).item()
        predicted_token = tokenizer.convert_ids_to_tokens([predicted_index])[0]
        
        if '#' not in gold: 
            i+=1
            if predicted_token == gold:
                count+=1
                print(tokenized_text)
                print(gold, predicted_token)
    print(count / i)