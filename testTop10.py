# Load the word-vector dictionary

import sys
sys.path.insert(0, '../')
from wordvecs import WordVecsDict

dic = WordVecsDict()

# Can use word2vec or GloVe vectors
dictFileName = '/vector-space-analogy-analysis/dicts/word2vec-GoogleNews-vecs300-norm.pickle'
# dictFileName = 'contextVectors.pickle'
dic.loadDict(dictFileName)

import cPickle

# Evaluate the model (word2vec or GloVe) on the relational similarity ratings
# for each relational type

import numpy as np
from numpy.linalg import norm
from scipy.stats import pearsonr

top_n = 50

# Get the ratings and predictions for each relational type

with open('sentences.txt') as sentences:
    # vocab = [word for word in line.strip('\n').split() for line in sentences]
    count = 0
    i = 0

    lines = sentences.readlines()

    for line in lines:
        items = line.split()

        pair1 = (items[0],items[3])
        pair2 = (items[6],items[9])

        if dic.hasWords(*pair1) and dic.hasWords(*pair2):
            predVec = dic.getDvec(pair1[0],pair1[1],pair2[0])

            topWords = dic.getClosestWords(predVec, num_results=top_n, similarity='cosine')
            words = [item[0].encode("utf-8") for item in topWords]
            if items[9] in words:
                count +=1
            i +=1
    print(count/float(i))
    print(i)