ratingsFileName = './vector-space-analogy-analysis/relsim/relsim_mean_ratings.csv'

with open(ratingsFileName, 'r') as ratingsFile:
    lines = ratingsFile.readlines()[1:]

ratings = []
for line in lines:
    tokens = line.strip().split(',')
    # relation1,relation2,comparison_type,pair1_word1,pair1_word2,pair2_word1,pair2_word2,mean_rating,num_ratings
    ratings.append(tokens)

# Generate complete sentences
with open('./sentences.txt','w') as out:
    for tokens in ratings:
        out.write(tokens[3]+' is to '+tokens[4]+' is like '+tokens[5]+' is to '+tokens[6]+' .\n')

# Generate masked sentences
with open('./masked_sentences.txt','w') as out:
    for tokens in ratings:
        out.write(tokens[3]+' is to '+tokens[4]+' is like '+tokens[5]+' is to @ .\n')
