## CIS700 - Commonsesense Reasoning AI - Project 2

### Files:

Note: The code contains files that are a mix of python2 and python3.  

#### Some useful scripts:
1) createSentences.py generates sentences with the metaphor dataset so we can create cotnextual embeddings with BERT.  
2) bert/extract_features.py to extract features from the creates sentences.  
3) filterFeatVecs.py extracts the context embedding from extracted features into a pickle file.  

#### Task 1:
vector-space-analogy-analysis/relsim/evaluate_model.ipynb is a jupyter notebook that evaluates model performance on the metaphor data set using context embeddings. (Python 2)  

#### Task 2:
predictMasked.py uses the output of createSentences.py to evaluate BERT's predictions on masked words in the metaphor sentences.  

#### Task 3:
testTop10.py uses the metaphor pairs and context embeddings to evaluate top-n predictions of BERT for missing words in metaphors.(Python 2)  
wordvecs.py is a helper script.  

#### Task 4:
createSarcasmDataset.py reformats and creates the dataset to for the classifciation script.  
sarcasmPrediction.ipynb notebook trains and evaluates the network for sarcasm classification task.  