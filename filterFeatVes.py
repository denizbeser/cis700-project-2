import json
import cPickle as pickle
import numpy as np

dic = {}

with open('featVecs.jsonl') as inf:
    for line in inf:
        items = json.loads(line)['features']
        for item in [items[1],items[4],items[7],items[10]]:
            word = item['token']
            vec = np.array(item['layers'][0]['values'])
            dic[word] = vec

pickle.dump(dic, open("contextVectors.pickle", "wb" ))