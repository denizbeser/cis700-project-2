import random
import os
random.seed(1)

sarcastic = []
neutral = []

with open('Task2_Sacarsm_TrainData_clean.txt', 'r') as inf:

    lines = inf.readlines()
    for i in range(len(lines)):
        line = lines[i]
        # 1 is sarcastic, 0 is not
        _, label, text = line.split('\t')
        text = text.replace('#sarcasm','')
        if label == '1':
            sarcastic.append(text)
        else:
            neutral.append(text)

random.shuffle(sarcastic)
random.shuffle(neutral)

#print(len(sarcastic))
#print(len(neutral))
#18488
#21292

sarcastic = sarcastic[:18000]
neutral = neutral[:18000]

if not os.path.exists(os.path.dirname('SarcasmDataset')):
    os.makedirs(os.path.dirname('SarcasmDataset/train/pos/'))
    os.makedirs(os.path.dirname('SarcasmDataset/train/neg/'))
    os.makedirs(os.path.dirname('SarcasmDataset/test/pos/'))
    os.makedirs(os.path.dirname('SarcasmDataset/test/neg/'))

for i in range(16000):
    with open('SarcasmDataset/train/pos/'+str(i)+'_1.txt','w+') as file:
        file.write(sarcastic[i])
    with open('SarcasmDataset/train/neg/'+str(i)+'_0.txt','w+') as file:
        file.write(neutral[i])

for i in range(16000,18000):
    with open('SarcasmDataset/test/pos/'+str(i)+'_1.txt','w+') as file:
        file.write(sarcastic[i])
    with open('SarcasmDataset/test/neg/'+str(i)+'_0.txt','w+') as file:
        file.write(neutral[i])